using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StateMachine;
/// <summary>
/// controller class for player character behaviour
/// is driven by a stateMachine that controls the differnt states the character can have
/// </summary>
public class PlayerController : MonoBehaviour
{
	#region state machine
	StateMachine<PlayerController> stateMachine;

    [SerializeField] BaseStateBehaviour<PlayerController> idleState;
    [SerializeField] BaseStateBehaviour<PlayerController> walkState;
    [SerializeField] BaseStateBehaviour<PlayerController> jumpState;
    [SerializeField] BaseStateBehaviour<PlayerController> fallState;

	[SerializeField] BaseStateBehaviour<PlayerController> currentState;
	public BaseStateBehaviour<PlayerController> IdleState { get => idleState; private set => idleState = value; }
	public BaseStateBehaviour<PlayerController> WalkState { get => walkState; private set => walkState = value; }
	public BaseStateBehaviour<PlayerController> JumpState { get => jumpState; private set => jumpState = value; }
	public BaseStateBehaviour<PlayerController> FallState { get => fallState; private set => fallState = value; }
	#endregion

	[SerializeField] Trigger groundedTrigger;
	public Trigger GroundedTrigger { get => groundedTrigger; private set => groundedTrigger = value; }
	public PlayerInput PlayerInput { get ; private set ; }
	public Movement Movement { get; private set; }
	public PlayerAnimator Animator { get; private set; }
	private void Start()
	{
		PlayerInput  = GetComponent<PlayerInput>();
		Movement = GetComponent<Movement>();
		Animator = GetComponent<PlayerAnimator>();
        stateMachine = new StateMachine<PlayerController>(this);

        stateMachine.AddState(idleState);
        stateMachine.AddState(jumpState);
        stateMachine.AddState(fallState);
        stateMachine.AddState(walkState);

        stateMachine.SetState(idleState);
	}

	private void Update()
	{
        PlayerInput.UpdateInput();
        stateMachine.Update();
		
		//just for inspector:
		currentState = (BaseStateBehaviour<PlayerController>)stateMachine.CurrentState;
	}
	private void FixedUpdate()
	{
		Movement.UpdateMovement();
	}
}
