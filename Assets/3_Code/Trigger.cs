using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
	[SerializeField] bool triggering = false;
	public bool Triggering { get => triggering; private set => triggering = value; }

	private void OnTriggerEnter(Collider other)
	{
		triggering = true;
	}
	private void OnTriggerExit(Collider other)
	{
		triggering = false;
	}
}
