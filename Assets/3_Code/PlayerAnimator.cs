using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    [SerializeField] Animator animator;

	[SerializeField] string idleParameter;
	[SerializeField] string junpParameter;
	[SerializeField] string fallParameter;
	[SerializeField] string walkParameter;

	public void SetIdle(bool val)
	{
		animator.SetBool(idleParameter, val);
	}
	public void SetJump(bool val)
	{
		animator.SetBool(junpParameter, val);
	}
	public void SetFall(bool val)
	{
		animator.SetBool(fallParameter, val);
	}
	public void SetWalk(bool val)
	{
		animator.SetBool(walkParameter, val);
	}
}
