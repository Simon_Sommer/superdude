using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine
{
	public class StateMachine<T>
	{
		public IState<T> CurrentState { get => currentState; set => currentState = value; }
		public T Context { get; private set; }

		private List<IState<T>> states = new List<IState<T>>();
		private IState<T> currentState;

		public StateMachine(T context)
		{
			Context = context;
		}

		void Awake()
		{
			//Context = GetComponent
		}
		public void Update()
		{
			if (currentState == null)
				return;

			currentState.UpdateState();
		}
		public void AddState(IState<T> state)
		{
			if (!states.Contains(state))
				states.Add(state);

			state.Create(this);
		}
		public void AddStates(IState<T>[] states)
		{
			foreach (IState<T> state in states)
			{
				if (!this.states.Contains(state))
					this.states.Add(state);

				state.Create(this);
			}
		}

		/// <summary>
		/// Sets the desired state as active state
		/// </summary>
		/// <param name="state"></param>
		/// <param name="resetState">Reset state when current state is equal to new State and call Enter() again</param>
		public void SetState(IState<T> state, bool resetState = true)
		{
			if (state == null)
			{
				Debug.LogWarning("[" + Context + "] : state not added > " + state + " !");
				return;
			}

			if (currentState != null)
			{
				if (!resetState && state == currentState)
					return;

				currentState.Exit();
			}
			currentState = state;

			//Debug.Log(this.Context);
			//if (this.Context.GetType() == typeof(PlayerController))
			//    Debug.Log("[" + Context + "] : entered state > " + currentState);

			state.Enter();
		}
	}
}