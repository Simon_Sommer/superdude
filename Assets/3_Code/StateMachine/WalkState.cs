﻿namespace StateMachine.Character
{
	//state class for when the player is moving on the ground
	public class WalkState : BaseStateBehaviour<PlayerController>
	{

		PlayerInput input;
		public override void Create(StateMachine<PlayerController> sm)
		{
			base.Create(sm);
			input = sm.Context.PlayerInput;
		}

		public override void Destroy()
		{
		}

		public override void Enter()
		{
			sm.Context.Movement.UseGravity = false;
			sm.Context.Animator.SetWalk(true);

		}

		public override void Exit()
		{
			sm.Context.Animator.SetWalk(false);
		}

		public override void UpdateState()
		{

			sm.Context.Movement.Move(input.Horizonzal, input.Vertical);

			if (input.Horizonzal == 0 && input.Vertical == 0)
			{
				sm.SetState(sm.Context.IdleState);
				return;
			}

			if (sm.Context.GroundedTrigger.Triggering == false)
			{
				sm.SetState(sm.Context.FallState);
				return;
			}

			if(input.Jump)
				sm.SetState(sm.Context.JumpState);
		}
	}
}