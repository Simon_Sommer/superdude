﻿using UnityEngine;
namespace StateMachine.Character
{
	/// <summary>
	/// state when the player starts jumping and is moving upwards
	/// </summary>
	public class JumpState : BaseStateBehaviour<PlayerController>
	{
		PlayerInput input;
		public override void Create(StateMachine<PlayerController> sm)
		{
			base.Create(sm);
			input = sm.Context.PlayerInput;
		}
		
		public override void Enter()
		{
			sm.Context.Movement.UseGravity = true;
			sm.Context.Movement.StopFalling();
			sm.Context.Movement.Jump();
			sm.Context.Animator.SetJump(true);
		}

		public override void Exit()
		{
			sm.Context.Animator.SetJump(false);
		}

		public override void UpdateState()
		{
			sm.Context.Movement.Move(input.Horizonzal, input.Vertical);

			if (sm.Context.Movement.CurrentYSpeed <= 0)
				sm.SetState(sm.Context.FallState);
		}
	}
}