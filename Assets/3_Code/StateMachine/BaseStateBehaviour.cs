using StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// base class for states that are used by a statemMchine
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class BaseStateBehaviour<T> : MonoBehaviour,IState<T>
{
    internal StateMachine<T> sm;
    public virtual void Create(StateMachine<T> sm)
    {
        this.sm = sm;
    }

    public virtual void Destroy()
    {
    }

    public virtual void Enter()
    {
    }

    public virtual void Exit()
    {
    }

    public virtual void UpdateState()
    {
    }
    #region Copy Paste Overrides
    //public override void Create(StateMachine<T> sm)
    //{
    //}

    //public override void Destroy()
    //{
    //}

    //public override void Enter()
    //{
    //}

    //public override void Exit()
    //{
    //}

    //public override void UpdateState()
    //{
    //}
    #endregion
}
