﻿namespace StateMachine.Character
{
	/// <summary>
	/// state class for when the player is in the air but falling
	/// </summary>
	public class FallState : BaseStateBehaviour<PlayerController>
	{
		PlayerInput input;
		public override void Create(StateMachine<PlayerController> sm)
		{
			base.Create(sm);
			input = sm.Context.PlayerInput;
		}

		public override void Destroy()
		{
		}

		public override void Enter()
		{
			sm.Context.Movement.UseGravity = true;
			sm.Context.Animator.SetFall(true);
		}

		public override void Exit()
		{
			sm.Context.Animator.SetFall(false);
		}

		public override void UpdateState()
		{
			sm.Context.Movement.Move(input.Horizonzal, input.Vertical);

			if (sm.Context.GroundedTrigger.Triggering)
				sm.SetState(sm.Context.IdleState);
		}
	}

}