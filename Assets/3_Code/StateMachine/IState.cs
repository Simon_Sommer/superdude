using StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IState<T>
{
    void Create(StateMachine<T> sm);
    void Destroy();
    void Enter();
    void Exit();
    void UpdateState();
}
