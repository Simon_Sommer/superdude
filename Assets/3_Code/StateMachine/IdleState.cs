using StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StateMachine.Character
{
	/// <summary>
	/// deafault player state for when the player is doing nothing
	/// </summary>
	public class IdleState : BaseStateBehaviour<PlayerController>
	{
		PlayerInput input;
		public override void Create(StateMachine<PlayerController> sm)
		{
			base.Create(sm);
			input = sm.Context.PlayerInput;
		}

		public override void Destroy()
		{
		}

		public override void Enter()
		{
			sm.Context.Movement.UseGravity = false;
			sm.Context.Movement.StopFalling();
			sm.Context.Animator.SetIdle(true);
		}

		public override void Exit()
		{
			sm.Context.Animator.SetIdle(false);
		}

		public override void UpdateState()
		{
			if (input.Vertical != 0 || input.Horizonzal != 0)
			{
				sm.SetState(sm.Context.WalkState);
				return;
			}

			if (sm.Context.PlayerInput.Jump)
			{
				sm.SetState(sm.Context.JumpState);
				return;
			}
			if (sm.Context.GroundedTrigger.Triggering == false)
			{
				sm.SetState(sm.Context.FallState);
				return;
			}
		}
	}

}