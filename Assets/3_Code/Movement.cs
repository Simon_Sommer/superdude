using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// class for player character movement
/// </summary>
public class Movement : MonoBehaviour
{
	[SerializeField] float moveSpeed = 10;
	[SerializeField] float jumpForce = 10;
	[SerializeField] float gravity = 1;

	[SerializeField] float currentYSpeed = 0;
	[SerializeField] float maxFallingSpeed = -20;
	float rotationThresshold = 0.1f;

	Vector3 moveVector;
	Rigidbody rBody;

	public bool UseGravity { get; set; }
	public float CurrentYSpeed { get => currentYSpeed; private set => currentYSpeed = value; }

	private void OnEnable()
	{
		rBody = GetComponent<Rigidbody>();
	}
	public void Jump()
	{
		currentYSpeed += jumpForce;
	}
	public void StopFalling()
	{
		currentYSpeed = Mathf.Max(0, currentYSpeed);
	}
	
	/// <summary>
	/// calculates the move vector from input, that is later applyed in the FixedUpdate
	/// </summary>
	/// <param name="xAxis"></param>
	/// <param name="zAxis"></param>
	public void Move(float xAxis, float zAxis)
	{
		moveVector.x = xAxis;
		moveVector.z = zAxis;
		moveVector.y = 0;
		moveVector.Normalize();
		moveVector *= moveSpeed;
	}
	
	/// <summary>
	/// adds gravity and applys the moveVector to the ridigbody, should be called in FixedUpdate
	/// </summary>
	public void UpdateMovement()
	{
		if (UseGravity)
			currentYSpeed = Mathf.Max(maxFallingSpeed, currentYSpeed - gravity * Time.deltaTime);

		moveVector.y = currentYSpeed;
		rBody.velocity = moveVector;

		Rotation();
	}
	void Rotation()
	{
		if (moveVector.sqrMagnitude < rotationThresshold)
			return;

		Vector3 dir = moveVector;
		dir.y = 0;
		transform.LookAt(transform.position + dir);
	}
}
