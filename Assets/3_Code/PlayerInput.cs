using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// this class contains simple input logic
/// </summary>
public class PlayerInput : MonoBehaviour
{
	bool jump = false;
	float vertical = 0;
	float horizonzal = 0;

	[SerializeField] KeyCode jumpKey;
	[SerializeField] KeyCode forewardKey;
	[SerializeField] KeyCode backwardKey;
	[SerializeField] KeyCode leftKey;
	[SerializeField] KeyCode rightKey;

	public float Vertical { get => vertical; private set => vertical = value; }
	public float Horizonzal { get => horizonzal; private set => horizonzal = value; }
	public bool Jump { get => jump; set => jump = value; }

	public void UpdateInput()
	{
		jump = Input.GetKeyDown(jumpKey);

		vertical = 0;
		if (Input.GetKey(forewardKey))
			vertical += 1;
		if (Input.GetKey(backwardKey))
			vertical -= 1;

		horizonzal = 0;
		
		if (Input.GetKey(leftKey))
			horizonzal += 1;
		if (Input.GetKey(rightKey))
			horizonzal -= 1;
	}
}
